# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] 2019-05-10
### Added
- Automatically delete merged automerge branches and set to merge when pipeline succeeds

### Fixed
- Include certificates in the Docker container to prevent HTTPS API requests failing
- Automerge branch naming so that re-running the automerge script on the same job will not fail if the automerge branch for that commit already exists

## [0.0.1] 2019-05-10
### Added
- Minimal automated merge between stable branches.

[Unreleased]: https://gitlab.com/jramsay/automerge-bot/compare/v0.0.1...HEAD
[0.0.2]: https://gitlab.com/jramsay/automerge-bot/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/jramsay/automerge-bot/releases/tag/v0.0.1
