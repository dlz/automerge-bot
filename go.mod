module automerge-bot

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hashicorp/go-version v1.1.0
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/xanzy/go-gitlab v0.15.0
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
)
