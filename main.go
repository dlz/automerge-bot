/*
Automatic merges for GitLab

Automated merges from stable/maintenance branches to master using GitLab CI to
create the merge request.
*/

package main

import (
	"errors"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
)

var (
	// Required configuration
	botAccessToken  = "GITLAB_ACCESS_TOKEN"
	botSemverPrefix = "SEMVER_PREFIX"

	// GitLab CI environment variables
	ciBaseURL   = "CI_API_V4_URL"
	ciProjectID = "CI_PROJECT_ID"
	ciBranch    = "CI_COMMIT_REF_NAME"
	ciCommit    = "CI_COMMIT_SHA"

	accessToken  string = os.Getenv(botAccessToken)
	semverPrefix string = os.Getenv(botSemverPrefix)
	apiURL       string = os.Getenv(ciBaseURL)
	projectID    string = os.Getenv(ciProjectID)
	sourceBranch string = os.Getenv(ciBranch)
	sourceCommit string = os.Getenv(ciCommit)
	targetBranch string

	// TODO: retrieve from API
	defaultBranch string = "master"
)

var (
	errBranchAlreadyExists = errors.New("branch already exists")
	errBranchRaceCondition = errors.New("branch race condition detected")
)

type SemverBranches []*SemverBranch

type SemverBranch struct {
	Name    string
	Version *version.Version
}

type MergeRequest struct {
	ID           int    `json:"id"`
	TargetBranch string `json:"target_branch"`
}

type Memo struct {
	MergeChain []*MergeRequest `json:"merge_chain"`
}

func main() {
	if apiURL == "" || projectID == "" || sourceBranch == "" || sourceCommit == "" {
		fmt.Println("ERROR: Expected GitLab CI environment variables to be set.")
		fmt.Println("Use outside of GitLab CI is not supported")
		os.Exit(1)
	}

	if accessToken == "" {
		fmt.Printf("ERROR: Expected %s to be set. API access is required to create new branches and merge requests.", botAccessToken)
		os.Exit(1)
	}

	client := gitlab.NewClient(nil, accessToken)
	client.SetBaseURL(apiURL)

	if err := automerge(sourceBranch, sourceCommit, projectID, client); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func automerge(sourceBranch, sourceCommit, projectID string, client *gitlab.Client) error {
	// Automated merges are only allowed from a semver branch to another semver
	// branch, or to the default branch.
	if isSemverBranch(sourceBranch) != true {
		return fmt.Errorf("ERROR: Expected semver source branch, got %s", sourceBranch)
	}

	semverBranches, err := listBranchesWithPrefix(client, semverPrefix)
	if err != nil {
		return err
	}

	targetBranch := nextSemverBranch(sourceBranch, semverBranches)

	fmt.Printf("Automatically merging from %s to %s, rippling towards %s\n", sourceBranch, targetBranch, defaultBranch)

	tempBranch := tempBranchName(sourceCommit, sourceBranch)
	if err := createBranch(client, tempBranch, sourceBranch, sourceCommit); err != nil {
		if err == errBranchAlreadyExists {
			fmt.Printf("Branch %s already exists, nothing to do.", tempBranch)
			os.Exit(0)
		}

		return err
	}

	// Example:
	//   stable-6.4: Prevent SQL injection
	title := fmt.Sprintf("%s: automerge from %s", targetBranch, sourceBranch)
	description := fmt.Sprintf("Automated merge from **%s** to **%s**\n\n", sourceBranch, targetBranch)

	mr, err := createMergeRequest(client, tempBranch, targetBranch, title, description)
	if err != nil {
		return err
	}

	if err := acceptMergeRequest(client, projectID, mr); err != nil {
		return err
	}

	return nil
}

// Lists branches that match the provided prefix.
func listBranchesWithPrefix(c *gitlab.Client, prefix string) (SemverBranches, error) {
	var branches []*SemverBranch
	opt := &gitlab.ListBranchesOptions{
		Page:    1,
		PerPage: 100,
	}

	for {
		bs, resp, err := c.Branches.ListBranches(projectID, opt, nil)
		if err != nil {
			return nil, err
		}

		for _, b := range bs {
			if strings.HasPrefix(b.Name, prefix) {
				ver, err := version.NewVersion(strings.TrimPrefix(b.Name, prefix))
				if err == nil {
					branches = append(branches, &SemverBranch{
						Name:    b.Name,
						Version: ver,
					})
				}
			}
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage

	}

	return SemverBranches(branches), nil
}

// Finds the merge request that created the current commit, assuming a push
func findMergeRequestForCommit(g *gitlab.Client, sha string) (*gitlab.MergeRequest, error) {
	var (
		c   *gitlab.Commit
		err error
	)

	// Fetch commit
	c, _, err = g.Commits.GetCommit(projectID, sha, nil)
	if err != nil {
		return nil, err
	}

	// merge commit, expect first commit from target and second from source
	if len(c.ParentIDs) == 2 {
		c, _, err = g.Commits.GetCommit(projectID, c.ParentIDs[1], nil)
		if err != nil {
			return nil, err
		}
	}

	if len(c.ParentIDs) > 2 {
		return nil, errors.New("Octopus merge detected! Please handle manually.")
	}

	// Fetch associated merge commits for commit or parent
	mrs, _, err := g.Commits.GetMergeRequestsByCommit(projectID, c.ID, nil)
	if err != nil {
		return nil, err
	}

	if len(mrs) == 0 {
		return nil, nil
	}

	if len(mrs) > 1 {
		fmt.Println("Multiple merge requests found...")
		for _, mr := range mrs {
			fmt.Printf("!%d %s\n", mr.ID, mr.Title)
		}
	}

	return mrs[0], nil
}

// Creates a branch from the current ref that can then be used to merge changes
// into the target branch.
func createBranch(g *gitlab.Client, branch string, ref string, sha string) error {
	opt := &gitlab.CreateBranchOptions{
		Branch: &branch,
		Ref:    &ref,
	}

	b, _, err := g.Branches.CreateBranch(projectID, opt, nil)
	if err != nil {
		if strings.Contains(err.Error(), "Branch already exists") {
			return errBranchAlreadyExists
		}
		return err
	}

	// FIXME: using the ref rather than the commit is a race condition, but isn't
	// significant problem. Using the commit to create a branch isn't possible via
	// the API yet. TODO: issues URL
	if b.Commit.ID != sha {
		return errBranchRaceCondition
	}

	return nil
}

func createMergeRequest(client *gitlab.Client, sourceBranch string, targetBranch string, title string, description string) (*gitlab.MergeRequest, error) {
	removeSourceBranch := true

	opt := &gitlab.CreateMergeRequestOptions{
		Title:              &title,
		Description:        &description,
		SourceBranch:       &sourceBranch,
		TargetBranch:       &targetBranch,
		RemoveSourceBranch: &removeSourceBranch,
	}

	mr, _, err := client.MergeRequests.CreateMergeRequest(projectID, opt, nil)
	if err != nil {
		return nil, err
	}

	return mr, nil
}

func acceptMergeRequest(client *gitlab.Client, projectID string, mr *gitlab.MergeRequest) error {
	mergeWhenPipelineSucceeds := true
	removeSourceBranch := true
	opt := &gitlab.AcceptMergeRequestOptions{
		MergeWhenPipelineSucceeds: &mergeWhenPipelineSucceeds,
		ShouldRemoveSourceBranch:  &removeSourceBranch,
	}

	_, _, err := client.MergeRequests.AcceptMergeRequest(projectID, mr.IID, opt, nil)
	if err != nil {
		return err
	}

	return nil
}

// Returns the next released version
func nextSemverBranch(v string, vs SemverBranches) string {

	// Sort list of candidates from lowest to highest
	sort.Sort(vs)

	for i, b := range vs[:len(vs)-1] {
		if b.Name == v {
			return vs[i+1].Name
		}
	}
	return defaultBranch
}

// Verify the current branch is a release branch
func isSemverBranch(b string) bool {
	_, err := version.NewVersion(strings.TrimPrefix(b, semverPrefix))
	if err != nil {
		return false
	}
	return true
}

// Create unique automerge branch name using the source commit so that there
// can be multiple merges between different release branches at the same time
// generated by different merges.
func tempBranchName(sourceCommit string, sourceRef string) string {
	return fmt.Sprintf("automerge-%s-%s", sourceRef, sourceCommit)
}

// Implements the sort.Interface interface so that branches can be sorted
func (bs SemverBranches) Len() int {
	return len(bs)
}

func (bs SemverBranches) Less(i, j int) bool {
	return bs[i].Version.LessThan(bs[j].Version)
}

func (bs SemverBranches) Swap(i, j int) {
	bs[i], bs[j] = bs[j], bs[i]
}
