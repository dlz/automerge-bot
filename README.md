# Automate merges between stable branches

In workflows where fixes are made to previous releases by fixing them in the oldest release, and rippling the change forward to the development branch, this script will automatically create branches and open corresponding merge requests.

In contrast with this approach, GitLab recommends an approach where changes are always merged into the develop branch and instead are ported to previous releases.

## Usage

Using GitLab CI, the following **CI variables** are required:

- `GITLAB_ACCESS_TOKEN` to create the new branch and merge request
- `SEMVER_PREFIX` to correctly locate the list of branches to consider for the next merge request

Add the following job to your `.gitlab-ci.yml`:

```yaml
stages:
  - pre-build

auto_merge:
  stage: pre-build
  image: registry.gitlab.com/jramsay/automerge-bot:0.0.2
  script:
    - automerge
  only:
    refs:
      - /^release-.*/  # the job will fail when run on non-release branches
  variables:
    GIT_STRATEGY: none # skip cloning since we'll use the API to create the merge request
```
